<?php

namespace Plumtree\LogincustProduct\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
	protected $formKey;   
	protected $cart;
	protected $product;
	private $productRepository; 

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\Data\Form\FormKey $formKey,
		\Magento\Checkout\Model\Cart $cart,
		\Magento\Catalog\Model\Product $product,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	array $data = []) {
	    $this->formKey = $formKey;
	    $this->cart = $cart;
	    $this->product = $product;     
	    $this->productRepository = $productRepository; 
	    parent::__construct($context);
	}
    public function execute()
    {

       /* $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();*/
        try{
        	$post = $this->getRequest()->getPost();
        	if($post['sku']){
        		$sku = $post['sku'];
        	}else{
        		$sku = "subscription-fastmodel";
        	}
	        $productData = $this->productRepository->get($sku);
	        $productId = $productData->getEntityId();
			$params = array(
			            'form_key' => $this->formKey->getFormKey(),
			            'product' => $productId, //product Id
			            'qty'   =>1 //quantity of product                
			        );              
			//Load the product based on productID   
			$_product = $this->product->load($productId);       
			$this->cart->addProduct($_product, $params);
			$this->cart->save();
			echo "1";
		}catch(Exception $e){
			echo $e->getMassage();exit;
		}
    }
}