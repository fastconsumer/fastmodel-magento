<?php
namespace Plumtree\Afterregister\Plugin\Magento\Customer\Model\Account;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

class Redirect
{
	protected $coreRegistry;
    protected $url;
    protected $resultFactory;

    public function __construct(Registry $registry, UrlInterface $url, ResultFactory $resultFactory)
    {
        $this->coreRegistry = $registry;
        $this->url = $url;
        $this->resultFactory = $resultFactory;
    }

    public function aroundGetRedirect(
        \Magento\Customer\Model\Account\Redirect $subject,
        \Closure $proceed
    ) {
        if ($this->coreRegistry->registry('is_new_account')) {
            /** @var \Magento\Framework\Controller\Result\Redirect $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setUrl($this->url->getUrl('afterregister'));
            return $result;
        }
        return $proceed();
    }
}

