define([
    "jquery",
	"matchMedia",
    "jquery/ui",
    "slickjs",
], function($, mediaCheck) {
    "use strict";

    //Creating CAP Slider Widget
    $.widget('fm.slider', {
		
		options: {
            selector: null,
			slickConfig: {},
        },
		
        _create: function () {
            var self = this;
			//this._super();
			
			//alert('createeeeeeee');
        },

        _init: function () {
			//this._super();
			var self = this;

			if( this.options.destroy != undefined) {
				if( this.options.destroy == true ) {
					this._slickDestroy(this.options);
					return false;
				}
			}
			
			if(typeof this.options.selector != '') {
				
				this.sScroll = 1;
				this.sShow = 5;
				this.autoSpeed = 3000;
				this.sShowTablet = 1;
				this.sShowMobile = 1;
				this.sShowPhone = 1;
				this.autoWidth = false;
				
				if( this.options.responsive != undefined) {
					this.sShowTablet = (this.options.responsive[0] != undefined) ? this.options.responsive[0] : this.sShowTablet;
					this.sShowMobile = (this.options.responsive[1] != undefined) ? this.options.responsive[1] : this.sShowMobile;
					this.sShowPhone = (this.options.responsive[2] != undefined) ? this.options.responsive[2] : this.sShowPhone;
				}	
				
				this.options.slickConfig = {
					variableWidth: (this.options.autoWidth != undefined) ? this.options.autoWidth : this.autoWidth,
					slidesToShow: (this.options.show != undefined) ? this.options.show : this.sShow,
					slidesToScroll: (this.options.scroll != undefined) ? this.options.scroll : this.sScroll,
					arrows: (this.options.arrows != undefined) ? this.options.arrows : false,
					dots: (this.options.dots != undefined) ? this.options.dots : false,
					infinite: (this.options.loop != undefined) ? this.options.loop : false,
					fade: (this.options.fade != undefined) ? this.options.fade : false,
					autoplay: (this.options.autoPlay != undefined) ? this.options.autoPlay : false,
					autoplaySpeed: (this.options.autoSpeed != undefined) ? this.options.autoSpeed : this.autoSpeed,
					touchMove: (this.options.touch != undefined) ? this.options.touch : true,
					adaptiveHeight: (this.options.autoHeight != undefined) ? this.options.autoHeight : false,
					responsive: [
					{
					  breakpoint: 993,
					  settings: {
						slidesToShow: this.sShowTablet,
						slidesToScroll: 1
					  }
					},
					{
					  breakpoint: 768,
					  settings: {
						slidesToShow: this.sShowMobile,
						slidesToScroll: 1
					  }
					},
					{
					  breakpoint: 421,
					  settings: {
						slidesToShow: this.sShowPhone,
						slidesToScroll: 1
					  }
					}
				  ]
				};
				
				
				
				if( this.options.lazyLoad != undefined) {
					if( this.options.lazyLoad == true ) {
						$(this.options.selector).find('img').each(function(i,e) {
							$(this).attr('data-lazy',$(this).attr('src')).removeAttr('src');
						});
						$.extend(this.options.slickConfig, { 'lazyLoad': 'ondemand' });
					}
				}
				
				//alert('------- ' + this.options.slickConfig.toSource());
				this._slickInit(this.options);

				//$(window).bind('resize '+ self.options.selector, self.options.selector, self._slickArrow).resize();
				
			}
		},
		
		_slickInit: function (obj) {
			$(obj.selector).slick(obj.slickConfig);
		},
		
		_slickDestroy: function (obj) {
			$(obj.selector).slick('unslick');
		},
		
		_slickArrow: function(obj) {
			$(obj.data + ' .slick-arrow').css({ 'top': ($(obj.data + ' .slick-slide .img, ' + obj.data + ' .slick-slide .image, ' + obj.data + ' .slick-slide .product-item-photo, ' + obj.data + ' .slick-slide .product-image').height() / 2) });
		}
		
	});
 
    //return $.fm.slider;
});