<?php

namespace Plumtree\ContributorSignup\Controller\Index;

use \Magento\Framework\Controller\ResultFactory;

class Subscribe extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_EMAIL_RECIPIENT_EMAIL = 'trans_email/ident_support/email';
	 /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_messageManager;
    protected $subscriberFactory;
    protected $_inlineTranslation;
    protected $_scopeConfig;
    protected $_transportBuilder;
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
        $this->_messageManager = $messageManager;
        $this->subscriberFactory= $subscriberFactory;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        parent::__construct($context);
    }

    public function execute()
    {
        try {

            $email  = $this->getRequest()->getParam('email');
            //$this->subscriberFactory->create()->subscribe($email);
            /*$this->_subscriberFactory->create()
                ->setStatus(Subscriber::STATUS_SUBSCRIBED)
                ->setEmail($email)
                ->save();*/
            // Send Mail
                $this->_inlineTranslation->suspend();
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                 
                $sender = [
                    'name' => 'fastmodel',
                    'email' => 'sales@fastmodel.com'
                ];
                
                
                $sentToEmail = $email;
                $sentToName = "fastmodel";
                /*admin email template*/ 

                $heading = 'Fast Model Subscribe'; 
                $subject = "Fast Model Subscribey";
                $message = 'Thank you for subscribe as a Newsletter.'; 
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('subscribe_email_template')
                    ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                    )
                    ->setTemplateVars([
                        'subject' => $subject,
                        'heading' => $heading,
                        'email'  => $email,
                        'message'  => $message
                    ])
                    ->setFrom($sender)
                    ->addTo($sentToEmail,$sentToName)
                    //->addTo('owner@example.com','owner')
                    ->getTransport();
                     
                $transport->sendMessage();
                $this->_inlineTranslation->resume();
                
            $this->_messageManager->addSuccessMessage('Thank you for subscribe as a Newsletter.');
            
        } catch (\Exception $e) {
             $this->_messageManager->addErrorMessage('Subscription failed. Please try again.');
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;        
    }
}