<?php

namespace Plumtree\Fastdrawlayout\Plugin\Catalog\Model\Layer\Filter;

class Item
{
    /**
     * @var \Magento\Catalog\Api\Data\CategoryInterfaceFactory
     */
    private $categoryFactory;

    /**
     * Item constructor.
     * @param \Magento\Catalog\Api\Data\CategoryInterfaceFactory $categoryFactory
     */
    public function __construct(
        \Magento\Catalog\Api\Data\CategoryInterfaceFactory $categoryFactory
    ) {
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Get filter item url
     *
     * @return string
     */
    public function aroundGetUrl(
        \Magento\Catalog\Model\Layer\Filter\Item $subject,
        \Closure $proceed
    ) {
        if($subject->getFilter()->getRequestVar() == 'cat') {
            $catId = $subject->getValue();
            $category = $this->categoryFactory->create()->load($catId);
            return $category->getUrl();
        }

        return $proceed();
    }
}