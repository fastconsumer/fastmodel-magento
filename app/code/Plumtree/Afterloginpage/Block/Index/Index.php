<?php

namespace Plumtree\Afterloginpage\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {
	protected $_customerSession ;
    protected $_currentUserWishlistCollectionFactory;
    protected $_productData;
    protected $_productcollection;
    protected $_productRepository;
    public function __construct(
    	\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
		\Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $currentUserWishlistCollectionFactory,
        \Magento\Catalog\Model\Product $productData,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Reports\Model\ResourceModel\Product\Collection $productcollection,
     	array $data = []) {
    	$this->request = $request;
        $this->customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;
        $this->_currentUserWishlistCollectionFactory = $currentUserWishlistCollectionFactory;
        $this->_productData = $productData;
        $this->_productRepository = $productRepository;
        $this->_productcollection = $productcollection;
        parent::__construct($context, $data);
    }


    protected function _prepareLayout()
    {
    	return parent::_prepareLayout();
    }

    public function getDetails(){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	    $customerSession = $objectManager->create('Magento\Customer\Model\Session');  
	    return $this->_customerSession->getCustomer();
    }

    public function getCustomerData(){
        return $this->_customerSession->getCustomer();
    }

    public function getWhishlist(){
    	$collection = $this->_currentUserWishlistCollectionFactory->create();
    	$collection->addCustomerIdFilter($this->_customerSession->getCustomerId());
        return $collection;
    }

    public function getProductViewedCount($id)
    {
        $productObj = $this->_productData->load($id);
        $this->_productcollection->setProductAttributeSetId($productObj->getAttributeSetId());
        $prodData = $this->_productcollection->addViewsCount()->getData();

        if (count($prodData) > 0) {
            foreach ($prodData as $product) {
                if ($product['entity_id'] == $id) {
                    return (int) $product['views'];
                }
            }
        }
        return 0;
    }

    public function loadProduct($id){
        $productObj = $this->_productData->load($id);
        return $productObj;
    }

    public function getProductUrl($productId){
        $product = $this->_productRepository->getById($productId);
        return $product->getUrlModel()->getUrl($product);
    }
}