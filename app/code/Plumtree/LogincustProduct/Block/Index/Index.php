<?php
namespace Plumtree\LogincustProduct\Block\Index;

class Index extends \Magento\Framework\View\Element\Template {

	protected $_productData;
	protected $request;

    public function __construct(
    	\Magento\Catalog\Block\Product\Context $context,
	    \Magento\Framework\App\Request\Http $request,
	    \Magento\Catalog\Model\Product $productData,
	    array $data = [])
	{
		$this->request = $request;
		$this->_productData = $productData;
        parent::__construct($context, $data);
    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function loadProduct($id){
        $productObj = $this->_productData->load($id);
        return $productObj;
    }

    public function getRelatedProduct()
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$productid = $this->request->getParam('pid');
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($productid);
		$relatedProducts = $product->getRelatedProducts();
		return $relatedProducts;
    }
}