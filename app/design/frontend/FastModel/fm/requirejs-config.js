var config = {
    map: {
        '*' : {
            'fm_scrolling'		: 'js/fm.scrolling',
            'fm_header'			: 'js/fm.header',
            'fm_footer'			: 'js/fm.footer',
            'fm_global'			: 'js/fm.global',
			'fm_slider' 		: 'js/fm.slick',
			'fm_bannerslider' 	: 'js/fm.bannerslider'
        }
    },
    paths: {
        'slickjs'		: 'js/vendor/slick.min',
		'fancyboxjs'	: 'js/vendor/fancybox.min',
		'equalheightjs'	: 'js/vendor/equalheights.min',
		'swiperjs'		: 'js/vendor/swiper.min'
    },
    shim: {
		fm_bannerslider: {
            deps: [
                'jquery'
            ]
        },
		fm_slider: {
            deps: [
                'jquery',
                'slickjs'
            ]
        },
        slickjs: {
			deps: [
				'jquery'
			]
		},
		fancyboxjs: {
            deps: [
				'jquery'
			]
        },
		equalheightjs: {
            deps: [
				'jquery'
			]
        },
		swiperjs: {
			deps: [
				'jquery'
			]
		}
    },
    deps: [
		'fm_scrolling',
		'fm_header',
		'fm_footer',
		'fm_global'
    ]
};
