<?php

namespace Plumtree\Afterregister\Controller\Index;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerFactory as CustomerResourceFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Save extends \Magento\Framework\App\Action\Action
{
	protected $customerResourceFactory;
    protected $customerModel;
    protected $customerSession;
    const CUSTOM_playbanktype = 'playbanktype';
    const CUSTOM_playbankgender = 'playbankgender';
    const CUSTOM_playbankregions = 'playbankstate';
    public function __construct(
        Context $context,
        CustomerResourceFactory $customerResourceFactory,
        Customer $customerModel,
        CustomerSession $customerSession
    )
    {
        parent::__construct($context);
        $this->customerResourceFactory = $customerResourceFactory;
        $this->customerModel = $customerModel;
        $this->customerSession = $customerSession;
       }

    public function execute()
    {
    	/*if ($this->getRequest()->getPost('playbanktype')):
    		if ($this->customerSession->isLoggedIn()) {
        	$playbanktypevalue=$this->getRequest()->getPost('playbanktype');
	        $customer = $this->_customerFactory->create()->load($customerId)->getDataModel();
		    $customer->setCustomAttribute('playbanktype', $playbanktypevalue);
		    $this->_customerRepositoryInterface->save($customer);
       	endif;*/
        //echo"ssssssssss";exit;
       	if ($this->customerSession->isLoggedIn()) {
            $customPlaybankType = $this->getRequest()->getPost('playbanktype');
            if($customPlaybankType){
            	
                try {
                    $customerId = $this->customerSession->getCustomer()->getId();
                    $customerNew = $this->customerModel->load($customerId);
                    $customerData = $customerNew->getDataModel();
                    $customerData->setCustomAttribute(self::CUSTOM_playbanktype, $customPlaybankType);
                    $customerNew->updateData($customerData);
                    $customerResource = $this->customerResourceFactory->create();
                    $customerResource->saveAttribute($customerNew, self::CUSTOM_playbanktype);
                    $this->messageManager->addSuccess(__('You are successfully save the data!!'));
                    //echo "1";exit;
                } catch (Exception $e) {
                    $this->messageManager->addError(__('Error to  save the data.'));
                }
	            
            }
            $customPlaybankGender = $this->getRequest()->getPost('playbankgender');
            if($customPlaybankGender){
                try {
                	$customerId = $this->customerSession->getCustomer()->getId();
    	            $customerNew = $this->customerModel->load($customerId);
    	            $customerData = $customerNew->getDataModel();
    	            $customerData->setCustomAttribute(self::CUSTOM_playbankgender, $customPlaybankGender);
    	            $customerNew->updateData($customerData);
    	            $customerResource = $this->customerResourceFactory->create();
    	            $customerResource->saveAttribute($customerNew, self::CUSTOM_playbankgender);
                    $this->messageManager->addSuccess(__('You are successfully save the data!!'));
                    //echo "1";exit;
                } catch (Exception $e) {
                    $this->messageManager->addError(__('Error to  save the data.'));
                }
            }
            $customPlaybankRegions = $this->getRequest()->getPost('playbankregions');
            if($customPlaybankRegions){
                try {
                    $customerId = $this->customerSession->getCustomer()->getId();
                    $customerNew = $this->customerModel->load($customerId);
                    $customerData = $customerNew->getDataModel();
                    $customerData->setCustomAttribute(self::CUSTOM_playbankregions, $customPlaybankRegions);
                    $customerNew->updateData($customerData);
                    $customerResource = $this->customerResourceFactory->create();
                    $customerResource->saveAttribute($customerNew, self::CUSTOM_playbankregions);
                    $this->messageManager->addSuccess(__('You are successfully save the data!!'));
                    //echo "1";exit;
                } catch (Exception $e) {
                    $this->messageManager->addError(__('Error to  save the data.'));
                }
            }

            
        }
    }
}