<?php
namespace Plumtree\Afterregister\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;

class Register implements \Magento\Framework\Event\ObserverInterface
{
	/*public function execute(\Magento\Framework\Event\Observer $observer)
	{
	 //$order= $observer->getData('order');
	 //$order->doSomething();
	 return $this;
	}*/
	protected $coreRegistry;

    public function __construct(Registry $registry)
    {
        $this->coreRegistry = $registry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->coreRegistry->register('is_new_account', true);
    }
}