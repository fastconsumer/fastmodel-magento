require([
    'jquery'
  ], function ($) {
    $(document).ready(function(){
        if ($("body").hasClass("catalog-category-view")){
            $( "#bannerslider" ).insertBefore($( ".columns .column.main" ) ); /*PLP page fix */     
        }
        $('#loginTabs li a:not(:first)').addClass('inactive');
        $('.login-container').hide();
        $('.login-container:first').show();
            
        $('#loginTabs li a').click(function(){
            var t = $(this).attr('id');
            if($(this).hasClass('inactive')){ /*this is the start of our condition*/ 
                $('#loginTabs li a').addClass('inactive');           
                $(this).removeClass('inactive');
                
                $('.login-container').hide();
                $('#'+ t + 'C').fadeIn('slow');
            }
        });

        /*Equalize Heights of Divs Start Here*/
	    $.fn.equalizeHeights = function(){
            return this.height( Math.max.apply(this, $(this).map(function(i,e){ return $(e).height() }).get() ) )
          }
        /*Equalize Heights of Divs End Here*/
    
        gridReset();
        $(window).on('load resize',function(){
            gridReset();
        });
        
        function gridReset() {
            $('.pricing-options-box .plan-cost').equalizeHeights(); 
            $('.pricing-options-box .plan .plan-features li').equalizeHeights();
        }
        if ($("body").hasClass("fastdraw")){
            $(".product.attribute.short-description" ).insertBefore($( ".product-add-form" ) ); /*PDP page fix */     
        }
        if ($("body").hasClass("live-streaming")){
            $(".product.attribute.short-description" ).insertBefore($( ".product-add-form" ) ); /*PDP page fix */     
        }
        if ($("body").hasClass("live-streaming")){
            $(".price_content" ).insertAfter($( ".product.attribute.short-description" ) ); /*PDP page fix */     
        }
        if ($("body").hasClass("live-streaming")){
            $(".vdo-pdp-social-buttons" ).insertAfter($( "body.live-streaming .product-options-bottom" ) ); /*PDP page fix */     
        }
        
    });
});

require(['jquery', 'fm_slider'], function ($) {
    var options = {
        selector: '#ccb-slider-box',
        responsive:	[4,3,2,1],
        arrows: true,
        dots: true,
        show: 4		
        }
    $.fm.slider(options);
});

require(['jquery', 'fm_slider'], function ($) {
    var options = {
        selector: '#rbs-slider-box',
        responsive:	[1],
        arrows: false,
        dots: true,
        show: 1		
        }
    $.fm.slider(options);
});

require(['jquery', 'fm_slider'], function ($) {
    var options = {
        selector: '#testimonial-slider',
        responsive:	[1],
        arrows: false,
        dots: true,
        show: 1		
        }
    $.fm.slider(options);
});


require([
    'jquery', 'fm_slider'
  ], function ($) {
    $(document).ready(function(){	    	
         $('.coache-slider').slick({
             slidesToShow: 1,
             slidesToScroll: 1,
             arrows: true,
             fade: false,
             asNavFor: '.slider-nav-thumbnails',
         });

         $('.slider-nav-thumbnails').slick({
             slidesToShow: 5,
             slidesToScroll: 1,
             asNavFor: '.coache-slider',
             dots: false,
             focusOnSelect: true,
             arrows: false
         });

         // Remove active class from all thumbnail slides
         $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

         // Set active class to first thumbnail slides
         $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

         // On before slide change match active thumbnail to current slide
         $('.coache-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
             var mySlideNumber = nextSlide;
             $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
             $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
        });
    });
});