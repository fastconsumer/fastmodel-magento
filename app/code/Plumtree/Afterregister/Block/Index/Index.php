<?php

namespace Plumtree\Afterregister\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {

	protected $states;
    protected $eavConfig; 
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
    \Magento\Directory\Model\RegionFactory $states,
    \Magento\Eav\Model\Config $eavConfig,
     array $data = []) {
        parent::__construct($context, $data);
        $this->states = $states;
        $this->eavConfig = $eavConfig; 
    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getRegions()
    {
    	$regions = $this->states->create()->getCollection()->addFieldToFilter('country_id','US'); 
    	return $regions;
    }

    public function getPlaybanktypeOptions()
    {
        $attribute = $this->eavConfig->getAttribute('customer', 'playbanktype');
        $options = $attribute->getSource()->getAllOptions();
        $optionsExists = array();
        foreach($options as $option) {
            $optionsExists[$option['value']] = $option['label'];
        }
        return $optionsExists;
    }

     public function getPlaybankgenderOptions()
    {
        $attribute = $this->eavConfig->getAttribute('customer', 'playbankgender');
        $options = $attribute->getSource()->getAllOptions();
        $optionsExists = array();
        foreach($options as $option) {
            $optionsExists[$option['value']] = $option['label'];
        }
        return $optionsExists;
    }
}