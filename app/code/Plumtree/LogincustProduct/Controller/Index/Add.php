<?php
namespace Plumtree\LogincustProduct\Controller\Index;


class Add extends \Magento\Framework\App\Action\Action
{
	protected $cart;
    protected $product;
    protected $_messageManager;
    protected $resultJsonFactory;

    public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Checkout\Model\Cart $cart,
    \Magento\Catalog\Model\Product $product,
    \Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    array $data = []) {
        $this->cart = $cart;
        $this->product = $product;    
        $this->_messageManager = $messageManager;  
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    { 
        $resultJson = $this->resultJsonFactory->create();
        try{
            $post = $this->getRequest()->getPost();
            $productId = $post['proId'];
            
            $additionalOptions['print_style'] = [
                'label' => 'Print Style',
                'value' => 'Test',
            ];

            $params = array(
                    'product' => $productId,
                    'qty' => 1,
                );
            $_product = $this->product->getById($productId);
            //$_product->addCustomOption('additional_options', serialize($additionalOptions));

            //Load the product based on productID   
            $this->cart->addProduct($_product, $params);
            $this->cart->save();

            $this->_messageManager->addSuccessMessage('Thank you for subscribe as a Contributor.');
            return $resultJson->setData(['success' => 'success']);

        } catch (\Exception $e) {
             $this->_messageManager->addErrorMessage('Product Add to Cart failed. Please try again.');
             return $resultJson->setData(['Error' => 'Fail']);
        }
        

        
        
    }
	
}
	
	