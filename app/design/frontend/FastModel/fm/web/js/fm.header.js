define([
    "jquery",
	"matchMedia",
    "jquery/ui",
	"domReady!"
], function($, mediaCheck) {
    "use strict";
	
	var self
	window.isBody = false;

    //creating header widget
    $.widget('fm.header', {
		options: {
        },
		
        _create: function () {
            self = this;
			
        },

        _init: function () {
			self = this;
			
		},
	});
 
    $.fm.header();
});