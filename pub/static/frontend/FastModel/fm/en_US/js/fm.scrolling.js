define([
    "jquery",
	"matchMedia",
    "jquery/ui",
], function($, mediaCheck) {
    "use strict";

	var self,
		globalPath	= location.href.split( '/' ), // Global Path by Location
		urlPath		= location.pathname, // Location Path Name
		urlPath_cms	= urlPath.split('/'), // Split Location Path Name by '/'
		protocol	= globalPath[0], // 'http/https' Protocol
		host		= globalPath[2], // 'host/domain' Name
		baseUrl		= protocol + '//' + host, // Base Url 
		hash		= location.hash, // Hash Params
		winWidth	= jQuery(window).width(), // Window Width
		screenWidth	= window.screen.width, // Screen Width
		
		html		= "html", // HTML
		body		= "body", // BODY
		main		= ".page-main", // MAIN DIV
		h			= ".header", // HEADER
		hc			= ".page-header", // HEADER CONTAINER
		hph			= ".page-header-placeholder", // HEADER PLACEHOLDER
		hsb			= ".header-ship-bar", // Ship Bar
		pw			= ".page-wrapper", // Page Wrapper
		pl			= ".page-lead", // Page Lead
		sb			= ".sidebar", // SideBar
		mm			= ".mobile-menu",
		mMenu 		= ".ms-megamenu",
		cs			= ".category-specific",
		tb			= ".tabs-bar",
		lv			= ".block-layered-nav",
		selectDd	= "select", // Select DropDown
		isSlider	= ".swiper-container", // Slider Swiper Container
		cTopImg		= ".top-container .category-image",
		cTopImgWrap = ".top-container .category-detail-wrap",
		topBan		= ".topbanner-container",
		topBanImg	= ".topbanner-container .image",
		topBanContent = ".topbanner-container .content",

		setTimer	= null,	// SetTimeout Timer
		pointerEvent = 'click', // Default Click EVent		
		isMobile	= '#mobile_check', // Mobile Check Selector
		isMobileCheck = false, // Mobile Check Bolean
		pScroll		= 0, // Page Sroll 0
		resizeCounter,	// 0 - Mobile / 1 - Desktop
		isScreen, isResponsive, cmsPage, scrollTop, scrollTop2, fixit;
	
    //creating scrolling widget
    $.widget('fm.scrolling', {
		
		options: {
            selector: null,
			config: {},
        },
		
        _create: function () {
            self = this;
			$(pw).prepend('<div class="page-header-placeholder" style="height:0;">&nbsp;</div>');
        },

        _init: function () {
			self = this;
			
			if( $(body).hasClass('catalog-product-view') ) {
				self.pageScroll = self._doHeaderSticky;
			} else {
				self.pageScroll = self._doHeaderSticky;
			}
			
			$(window).bind('scroll', self.pageScroll );
		},
		
		_doHeaderSticky: function() {
			self = this;
			
			pScroll = $(this).scrollTop();
			scrollTop = $(main).offset().top;
			
			if( $(pl).length > 0 ) {
				scrollTop = $(pl).offset().top;
			}
			
			if( pScroll > scrollTop ) {
				//console.log('if');
				$(hph).css({'display': 'block', 'height': scrollTop});
				$(hc).addClass('page-header-sticky');
				
				setTimer = setTimeout(function() {
					$(hc).addClass('ready');
				},100);
			} else {
				//console.log('else');
				if(pScroll <= 0) {
					$(hc).removeClass('page-header-sticky');
					$(hph).attr('style','');
					if (setTimer) {
						$(hc).removeClass('ready');
						clearTimeout(setTimer);
						setTimer = null;
					}
				}
			}
		}
		
	});
 
    $.fm.scrolling();
});