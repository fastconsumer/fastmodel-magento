<?php
namespace Plumtree\LogincustProduct\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

class Changecustomergroup implements ObserverInterface
{
	/**
     * @param Observer $observer
     * @return void
     */
	const CUSTOMER_GROUP_ID = 4;
    private $storeManager;
    protected $_checkoutSession;
    protected $customerSession;
    protected $_customerRepositoryInterface;

	public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
        $this->storeManager = $storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
    }
	
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
	    //$order= $observer->getData('order');
		//$order->doSomething();
	  	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/templog.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("Changecustomergroup");
       	if($this->customerSession->isLoggedIn()) {
       		$customerId = $this->customerSession->getCustomer()->getId();
       		$logger->info("customerId ".$customerId);
	        $grpId = $this->customerSession->getCustomer()->getGroupId(); 
	        $logger->info("grpId ".$grpId);
	        if($grpId != 4){
	       		$order = $observer -> getEvent() -> getOrder();
				$items = $order->getAllVisibleItems();
				foreach($items as $item){
					$sku = $item->getSku();
					$logger->info("sku ".$sku);
					if($sku = "subscription-fastmodel"){
						$logger->info("sku44444 ".$sku);
						$customer = $this->_customerRepositoryInterface->getById($customerId);
						$logger->info("44444444 ".$customer->getGroupId());
			            $customer->setGroupId(self::CUSTOMER_GROUP_ID);
						$this->_customerRepositoryInterface->save($customer);
					}
				}
       		}
		}
	    return $this;
	}
}