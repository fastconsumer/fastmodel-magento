<?php

namespace Plumtree\Afterloginpage\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_customerSession;
	protected $_objectManager;
	protected $_resultPageFactorys;

	public function __construct(
	    \Magento\Framework\App\Action\Context $context,
	    \Magento\Customer\Model\Session $customerSession,  
	    \Magento\Framework\ObjectManagerInterface $objectManager,
	    \Magento\Framework\View\Result\PageFactory $resultPageFactorys
	) {
	    parent::__construct($context);
	    $this->_customerSession = $customerSession;
	    $this->_objectManager = $objectManager;
	    $this->_resultPageFactorys = $resultPageFactorys;
  	}
  	
    public function execute()
    {
    	$customerData = $this->_customerSession->getCustomer()->getId();
	    $resultPageFactory = $this->_resultPageFactorys->create();
	    $resultPageFactory->getConfig()->getTitle()->set(__('My Account'));
	     $breadcrumbs = $resultPageFactory->getLayout()->getBlock('breadcrumbs');
            $breadcrumbs->addCrumb('home', [
                'label' => __('Fastmodelsports.com'),
                'title' => __('Fastmodelsports.com'),
                'link' => $this->_url->getUrl('')
                    ]
            );
    	if($customerData){
	        $this->_view->loadLayout();
	        $this->_view->getLayout()->initMessages();
	        $this->_view->renderLayout();
    	}else{
    		//echo"Please login";
    		$urlInterface = $this->_objectManager->get('\Magento\Framework\UrlInterface');
    		$this->_customerSession->setAfterAuthUrl($urlInterface->getCurrentUrl());
    		$this->_customerSession->authenticate();
    	}
    }

}