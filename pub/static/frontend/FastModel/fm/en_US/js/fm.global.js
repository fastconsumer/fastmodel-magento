define([
    "jquery",
	"matchMedia",
	"ko",
    "jquery/ui",
    "domReady!",
], function($, mediaCheck, ko) {
    "use strict";
 
	var self;
	
    //creating global widget
    $.widget('fm.global', {
		options: {
        },
		
        _create: function () {
            self = this;
			//alert('global');
        },

        _init: function () {
            self = this;
            
			self._pdpLinksScroll();
			
		},
		
		_pdpLinksScroll: function() {
			var sectionScroll,
				sectionItem;
				
			$('.fm-product-scroll-links a').on('click',function(e) {
				e.preventDefault();
				if( $(this).hasClass('sll-nvd') ) {
					sectionItem = '.attrib-video';
				} else if( $(this).hasClass('sll-fme') ) {
					sectionItem = '.attrib-twitter';
				} else if( $(this).hasClass('sll-plays') ) {
					sectionItem = '.block.related';
				} else if( $(this).hasClass('sll-tags') ) {
					sectionItem = '.attrib-tags';
				}
				
				sectionScroll = ($(sectionItem).length) ? $(sectionItem).offset().top : 0;
				
				if(sectionScroll > 0) {
					$('body,html').animate({
						scrollTop: sectionScroll - $('.page-header').height()
					},800);
				}
			});
		}
	});
 
	$.fm.global();
});