<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Smtp
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Plumtree\Catalog\Block;
use Magento\Catalog\Block\Product\ImageBuilder;

class Pdpdetails extends \Magento\Framework\View\Element\Template
{
    public $_storeManager;
    protected $_registry;
    protected $_product;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product $product,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_registry = $registry;
        $this->_product = $product;
        parent::__construct($context);
    }

    public function sayHello()
    {
        return __('Hello World');
    }

    public function getMediaUrl(){
       return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getBaseUrl(){
       return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getCurrentProductId()
    {        
        return $this->_registry->registry('current_product')->getId();
    }

    public function getCurrentPageView()
    {        
        $products = $this->_product->load($this->getCurrentProductId());
        //echo $product->getMetalPads();
        return $products->getAttributeText('page_view');;//$this->_registry->registry('current_product')->getAttributeText('page_view');
    }

    public function getCurrentProductSku()
    {        
        return $this->_registry->registry('current_product')->getSku();
    }  
}