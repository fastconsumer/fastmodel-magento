<?php
namespace Plumtree\General\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const FAST_MODEL_SPORTS = 1;

	protected $_category;
	protected $_session;
	protected $_product;
	protected $_categoryRepositoryInterface;
	protected $_storeManager;
	protected $_registry;
	protected $_order;
	protected $_listProduct;
	protected $checkoutCart;
	protected $summeryCount;	
	protected $mediaDirectory;
	protected $filesystem;
	protected $imageFactory;
	protected $configurable;
	protected $catalogSession;	
	protected $_customerSession;
	protected $categoryFactory;

	/**
	 * @param \Magento\Framework\App\Helper\Context $context
	 */
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Checkout\Model\Session $session,
		\Magento\Catalog\Model\ProductRepository $product,
		\Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepositoryInterface,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Registry $registry,
		\Magento\Sales\Api\Data\OrderInterface $order,
		\Plumtree\General\Block\Catalog\Product\ListProduct $listProduct,
		\Magento\Catalog\Model\Category $category,
		\Magento\Checkout\Model\Cart $checkoutCart,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\Image\AdapterFactory $imageFactory,
		\Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
		\Magento\Catalog\Model\Session $catalogSession,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Customer\Model\Session $customerSession  
	){ 
		parent::__construct($context);
		$this->_listProduct = $listProduct;
		$this->_category = $category;
		$this->_session = $session;
		$this->_product = $product;
		$this->_categoryRepositoryInterface = $categoryRepositoryInterface;
		$this->_storeManager = $storeManager;
		$this->_registry = $registry;
		$this->_order = $order;
		$this->checkoutCart = $checkoutCart;
		$this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
		$this->filesystem = $filesystem; 
		$this->imageFactory = $imageFactory;
		$this->configurable = $configurable;
		$this->catalogSession = $catalogSession;
		$this->_customerSession = $customerSession;
		$this->categoryFactory = $categoryFactory;
	}

	/**
	 * Get the collection of specic category
	 * product collection
	 *
	 * @param number $categoryId
	 * @param number $limit
	 */
	public function getSpecificCategoryCollection($categoryId = 0, $limit = 0)
	{
		if( (int)$categoryId === 0 )
			return false;

		$category = $this->categoryFactory->create()->load($categoryId);

		if($category->getId()) {
			$productCollection = $category->getProductCollection();
			$productCollection->addAttributeToSelect('*')
								->addAttributeToFilter('status', 1)
								->addAttributeToFilter('visibility', array(2,4))
								->addAttributeToSort('position', 'ASC');
			if((int)$limit > 0)
				$productCollection->getSelect()->limit($limit);

			if( count($productCollection) > 0) {
				return $productCollection;
			}	
		}

		return false;		
	}

	/**
	 * Get the level 2 categories collections
	 *
	 * @param number $level
	 * @return 	 \Magento\Catalog\Model\Category object | boolean
	 */
	public function getCategoriesByLevel($level = 2, $order = "ASC")
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$storeInstance = $objectManager->create('Magento\Store\Model\StoreManagerInterface');
		$storeInstance = $storeInstance->getStore();

		$categoryCollection = $this->_category->getCollection()->addAttributeToSelect('*')
								->addIsActiveFilter()
								->addFieldToFilter('path', array('like' => "1/{$storeInstance->getRootCategoryId()}/%"))
								->addFieldToFilter('level', $level)
								->addOrder("position", $order);
		
		if( $categoryCollection->count() > 0 )
			
			return $categoryCollection;
		return false;
	}

	public function getChildCategories($id = 0, $limit = 0, $order = "ASC")
	{
		if( $id == 0)
			return false;

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeInstance = $objectManager->create('Magento\Store\Model\StoreFactory');
			$storeInstance = $storeInstance->create()->load(self::FAST_MODEL_SPORTS);

			$collection = $this->_category->getCollection()
								->addAttributeToSelect("*")
								->addIsActiveFilter()->addFieldToFilter("parent_id", $id);
			if((int)$limit > 0)
				$collection->getSelect()->limit($limit);
			$collection->addOrder("position", $order);
			if( $collection->count() )
				return $collection;
			return false;
	}
	
	public function getSpecificCategories($ids = 0, $order = "ASC")
	{
		
		if( $ids == 0)
			return false;

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$storeInstance = $objectManager->create('Magento\Store\Model\StoreFactory');
			$storeInstance = $storeInstance->create()->load(self::FAST_MODEL_SPORTS);

			$collection = $this->_category->getCollection()
								->addAttributeToSelect("*")
								->addIsActiveFilter()
								->addFieldToFilter("entity_id", array('in' => $ids));
			
			$collection->addOrder("position", $order);

			if( $collection->count() )
				return $collection;
			return false;
	}
	
	public function getSpecificProducts($ids = 0, $order = "ASC")
	{
		if( $ids == 0)
			return false;
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

		$collection = $productCollection->addAttributeToSelect('*')
										->addAttributeToFilter('status', 1)
										->addAttributeToFilter('visibility', array(2,4))
										->addFieldToFilter('entity_id', array('in' => $ids));
	
		$collection->addOrder("position", $order);

		if( $collection->count() )
			return $collection;
		return false;
	}

	public function getQuote() {
	    return $this->_session->getQuote();
	}

	public function getProduct($id) {
		return $this->_product->getById($id);
	}

	public function getCategory($id) {
		$category = $this->_categoryRepositoryInterface->get($id, $this->_storeManager->getStore()->getId());
        return $category;
	}

    public function getCurrentCategory() {
        return $this->_registry->registry('current_category');
    }

    public function getCurrentProduct() {
        return $this->_registry->registry('current_product');
    }

    public function getOrder($id) {
    	return $this->_order->loadByIncrementId($id);
    }

    public function getProductUrl(\Magento\Catalog\Model\Product $product)
    {
    	return $this->_listProduct->getAddToCartPostParams($product);
    }

    /**
     * Get shopping cart items qty based on configuration (summary qty or items qty)
     *
     * @return int|float
     */
    public function getSummaryCount()
    {        
		$this->summeryCount = $this->checkoutCart->getSummaryQty() ?: 0;        
        return $this->summeryCount;
    }

    public static function getFrontWebsitesArray(){
        return array(self::FAST_MODEL_SPORTS);
    }

    public function getCartCountItems()
    {        
		return $this->checkoutCart->getItemsCount();
    }

	public function getParentIdsByChild($id) {
		return $this->configurable->getParentIdsByChild($id);
	}

	public function getMediaUrl() {
        $mediaUrl = $this->_storeManager
        					->getStore()
        					->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
		return $mediaUrl;
	}

	public function getCatalogSession() {
		return $this->catalogSession;
	}

	public function getStoreManager() {
		return $this->_storeManager;
	}			

	public function getCurrentCategoryId() {
        $category = $this->_registry->registry('current_category');
		return $category->getId();
    }
    public function getCurrentCategoryName() {
        $category = $this->_registry->registry('current_category');
		return $category->getName();
    }
    public function getCurrentCategoryDescription() {
        $category = $this->_registry->registry('current_category');
		return $category->getDiscription();
    }

    public function showSignUpButton() {
    	if (strpos($_SERVER['REQUEST_URI'], 'playbank.html')){
        	return true;
		}
		return false;
    }

    public function getContributorUrlAction() {
    	return $this->_storeManager->getStore()->getBaseUrl().'contributorsignup'; 	
	}

	public function isLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }
    public function getCategoryLandingId() {
        $category = $this->_registry->registry('current_category');
		return $category->getLandingPage();
    }
}
