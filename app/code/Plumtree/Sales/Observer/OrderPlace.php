<?php
namespace Plumtree\Sales\Observer;
class OrderPlace implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
     	$writer = new \Zend\Log\Writer\Stream(BP . '/order.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$order = $observer->getEvent()->getOrder();
		$orderid = $order->getId();
    	$order_number = $order->getIncrementId();
    	$customerId = $order->getCustomerId();
    	$status = $order->getStatus();
		$custLastName= $order->getCustomerLastname();
		$custFirsrName= $order->getCustomerFirstname();
		$ipaddress=$order->getRemoteIp();
		$customer_email=$order->getCustomerEmail();
 
		$billingaddress=$order->getBillingAddress();
		$billingcity=$billingaddress->getCity();      
		$billingstreet=$billingaddress->getStreet();
		$billingpostcode=$billingaddress->getPostcode();
		$billingtelephone=$billingaddress->getTelephone();
		$billingstate_code=$billingaddress->getRegionCode();
		
		$tax_amount=$order->getTaxAmount();
  		$total=$order->getGrandTotal(); 

  		$payment = $order->getPayment();
  		$method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle();

		$data = 'order_number:'.$order_number.',total:'.$total.',tax_amount:'.$tax_amount.',methodTitle:'.$methodTitle.',customerId:'.$customerId.',custLastName:'.$custLastName.',custFirsrName:'.$custFirsrName.',customer_email:'.$customer_email.',status:'.$status;

		//.',billingcity:'.$billingcity.',billingstreet:'.$billingstreet.',billingpostcode:'.$billingpostcode.',billingtelephone:'.$billingtelephone.',billingstate_code:'.$billingstate_code.',status:'.$status;

		//$string_version = implode(',', $data);
		$logger->info($data);
     
    }
}