<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Plumtree\General\Block\Catalog\Product;

/**
 * Product list
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{	
	
	/**
	 * Get post parameters
	 *
	 * @param \Magento\Catalog\Model\Product $product
	 * @return string
	 */
	public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
	{
		$url = $this->getAddToCartUrl($product);
		if( (string)$product->getTypeId() === "configurable"){
			$url = $this->_cartHelper->getAddUrl($product);
		}
		
		return [
			'action' => $url,
			'data' => [
				'product' => $product->getEntityId(),
				\Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
				$this->urlHelper->getEncodedUrl($url),
			]
		];
		
	}
}
