<?php
namespace Plumtree\Afterregister\Model\Source;
class Regions extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $states;
    protected $_options;

    public function __construct(\Magento\Catalog\Block\Product\Context $context,
    \Magento\Directory\Model\RegionFactory $states, array $data = []) {
        $this->states = $states;
    }

     
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $regionsArray = array();
            $regions = $this->states->create()->getCollection()->addFieldToFilter('country_id','US');
            foreach ($regions as $key => $values) {
                $regionsArray[] = [
                    'value' => $values['region_id'],
                    'label' => $values['name']
                ];
            } 
            $this->_options = $regionsArray;
           /* $this->_options = [
                ['value' => '0', 'label' => __('No')],
                ['value' => '1', 'label' => __('Yes')]
            ];*/
        }
        return $this->_options;
    }
    final public function toOptionArray()
    {
        $regions = $this->states->create()->getCollection()->addFieldToFilter('country_id','US');
        $regionsArray = array();
        foreach ($regions as $key => $values) {
           $regionsArray[] = [
                'value' => $values['region_id'],
                'label' => $values['name']
            ];
        } 
       return $regionsArray;
       /*return array(
            array('value' => '0', 'label' => __('No')),
            array('value' => '1', 'label' => __('Yes'))
        );*/
   }


    /*public static function getOptionArray()
    {
        $regions = $this->states->create()->getCollection()->addFieldToFilter('country_id','US');
        $regionsArray = array();
        foreach ($regions as $key => $values) {
           $regionsArray[] = [
                'value' => $values['region_id'],
                'label' => $values['name']
            ];
        } 
        print_r($regionsArray);exit;
        return $regionsArray;
    }*/

   
}
?>