<?php

namespace Plumtree\ContributorSignup\Controller\Index;

use \Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
	 /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    protected $_messageManager;

    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory    $customerFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->storeManager     = $storeManager;
        $this->customerFactory  = $customerFactory;
         $this->_messageManager = $messageManager;

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $firstname  = $this->getRequest()->getParam('firstname');
            $lastname  = $this->getRequest()->getParam('lastname');
            $password  = $this->getRequest()->getParam('password');
            $email  = $this->getRequest()->getParam('email');

            // Get Website ID
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

            // Instantiate object (this is the most important part)
            $customer   = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);

            // Preparing data for new customer
            $customer->setEmail($email); 
            $customer->setFirstname($firstname);
            $customer->setLastname($lastname);
            $customer->setPassword($password);
            $customer->setGroupId(5);

            // Save data
            $customer->save();
            #$customer->sendNewAccountEmail();
            $this->_messageManager->addSuccessMessage('Thank you for subscribe as a Contributor.');
            
        } catch (\Exception $e) {
             $this->_messageManager->addErrorMessage('Subscription failed. Please try again.');
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;        
    }
}