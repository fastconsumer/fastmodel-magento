define([
    "jquery",
	"matchMedia",
    "jquery/ui",
    "domReady!",
], function($, mediaCheck) {
    "use strict";
 
	var self;
	
    //creating footer widget
    $.widget('fm.footer', {
		options: {
			back2top: true
        },
		
        _create: function () {
            self = this;
			
			if (self.options.back2top === true) {
				self._backToTop();
			}
        },

        _init: function () {
            self = this;
			
		},
		
		_backToTop: function() {
			
			$('body').append('<div id="back2top"><a href="#" style="display: none"><i class="ic ic-arrow-up"></i><br>top</a></div>');
			
			$(document).on('click', '#back2top > a', function(e){
				e.preventDefault();
				
				$('body,html').animate({
					scrollTop:0
				},800);
			});
			
			$(window).scroll(function(e){
				if($(this).scrollTop() > $(window).height()){
					$('#back2top > a').fadeIn();
				}else{
					$('#back2top > a').fadeOut();
				}
			});
		},
	});
 
    $.fm.footer();
});