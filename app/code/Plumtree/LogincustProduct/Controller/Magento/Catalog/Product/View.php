<?php
namespace Plumtree\LogincustProduct\Controller\Magento\Catalog\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends \Magento\Catalog\Controller\Product\View
{
	 /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */

    Protected $customerSession;
    protected $urlInterface;
    protected $registry;

    public function __construct(
        Context $context,
        \Magento\Catalog\Helper\Product\View $viewHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Registry $registry,
        PageFactory $resultPageFactory
    ) {
        $this->viewHelper = $viewHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession=$customerSession;
        $this->urlInterface = $url;
        $this->registry = $registry;
        parent::__construct($context, $viewHelper, $resultForwardFactory,$resultPageFactory);
    }

    protected function noProductRedirect()
    {
        $store = $this->getRequest()->getQuery('store');
        if (isset($store) && !$this->getResponse()->isRedirect()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('');
        } elseif (!$this->getResponse()->isRedirect()) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        }
    }

    public function execute()
    {
       
        try{
            if($this->customerSession->isLoggedin())
            {
                if($this->customerSession->getCustomer()->getGroupId() == 4)
                {
                    $categoryId = (int) $this->getRequest()->getParam('category', false);
                    $productId = (int) $this->getRequest()->getParam('id');
                    $specifyOptions = $this->getRequest()->getParam('options');

                    if ($this->getRequest()->isPost() && $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
                        $product = $this->_initProduct();
                        if (!$product) {
                            return $this->noProductRedirect();
                        }
                        if ($specifyOptions) {
                            $notice = $product->getTypeInstance()->getSpecifyOptionMessage();
                            $this->messageManager->addNotice($notice);
                        }
                        if ($this->getRequest()->isAjax()) {
                            $this->getResponse()->representJson(
                                $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode([
                                    'backUrl' => $this->_redirect->getRedirectUrl()
                                ])
                            );
                            return;
                        }
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setRefererOrBaseUrl();
                        return $resultRedirect;
                    }

                    $params = new \Magento\Framework\DataObject();
                    $params->setCategoryId($categoryId);
                    $params->setSpecifyOptions($specifyOptions);

                    try {
                        $page = $this->resultPageFactory->create();
                        $this->viewHelper->prepareAndRender($page, $productId, $this, $params);
                        return $page;
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                        return $this->noProductRedirect();
                    } catch (\Exception $e) {
                        $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                        $resultForward = $this->resultForwardFactory->create();
                        $resultForward->forward('noroute');
                        return $resultForward;
                    }
                }else{
                    $productin = $this->_initProduct();
                    /*$this->customerSession->setAfterAuthUrl($this->_url->getCurrentUrl());
                    $this->customerSession->authenticate();*/
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $productData = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
                    $productId = $productData->getId();
                    $this->customerSession->setProdictSid($productId);
                    $subcribe_url = $this->urlInterface->getUrl('subcribe-fastmodel?pid='.$productId);
                    return $this->resultRedirectFactory->create()->setPath($subcribe_url);
                }
            }else{
                if($this->getCurrentProductTest() == 1){
                    return parent::execute();    
                }                
                $this->customerSession->setAfterAuthUrl($this->_url->getCurrentUrl());
                $this->customerSession->authenticate();
            }
        }catch(Exception $e){

        }
        return parent::execute();
    }
    /*Get product*/
    public function getCurrentProductTest()
    {        
        $productId = (int) $this->getRequest()->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        
        return $product->getWithoutlogin();
    }
	
}
	
	