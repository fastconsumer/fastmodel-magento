define([
    "jquery",
	"matchMedia",
    "domReady!",
], function($, mediaCheck) {
    "use strict";

    $.widget('fm.bannerslider', {
		
		options: {
			selector: null,
			imageSrc: 'src',
			imageDesktop: 'data-des',
            imageMobile: 'data-mob',
			breadcrumbs: '.breadcrumbs'
        },
		
        _create: function () {
            var self = this;
			
        },

        _init: function () {
			var self = this;
			
			if (this.options.selector != null) {
				this._bannerSliderInit();
			}
		},
		
		_bannerSliderInit: function() {
			mediaCheck({
				media: '(max-width: 767px)',
				entry: $.proxy(function () {
					this._bannerSliderMobile();
				}, this),
				exit: $.proxy(function () {
					this._bannerSliderDesktop();
				}, this)
			});
		},
		
		_bannerSliderDesktop: function () {
			//console.log('- des');
			var self = this;
			//if( $(self.options.selector + ' .bannerslide').hasClass('is-large') ) {
				$(self.options.selector + ' .bannerslide').removeClass('is-stretch-mob');
			//}
			//$(self.options.selector + ' .image').each(function(i) {
			$(self.options.selector + ' .bannerslide').each(function(i) {
				//console.log('- ' + i + ' -- ' + $(this).find('img').attr(self.options.imageDesktop));
				$(this).find('image img').attr(self.options.imageSrc, $(this).find('image img').attr(self.options.imageDesktop));
			});
			//$(this.options.selector + ' .image img').attr(this.options.imageSrc, this.options.imageDesktop);
		},
		
		_bannerSliderMobile: function () {
			//console.log('- mob');
			var self = this;
			//if( $(self.options.selector + ' .bannerslide').hasClass('is-large') ) {
				$(self.options.selector + ' .bannerslide').addClass('is-stretch-mob');
			//}
			//$(self.options.selector + ' .image').each(function(i) {
			$(self.options.selector + ' .bannerslide').each(function(i) {
				//console.log('- ' + i + ' -- ' + $(this).find('img').attr(self.options.imageMobile));
				$(this).find('image img').attr(self.options.imageSrc, $(this).find('image img').attr(self.options.imageMobile));
			});
			//$(this.options.selector + ' .image img').attr(this.options.imageSrc, this.options.imageMobile);
		}
		
	});
 
    return $.fm.bannerslider;
});